#include <stdbool.h>

#include "counter.h"

void init(counter c) {
/*
    Needs implementation.
*/
    if (c != 0) { // just nonsense to make the compiler happy
        c = 0;
    }
}

void inc(counter c) {
/*
    Needs implementation.
*/
    c++;
}

void dec(counter c) {
/*
    Needs implementation.
*/
    c--;
}

bool is_init(counter c) {
/*
    Needs implementation.
*/
    return (c == 0);
}

