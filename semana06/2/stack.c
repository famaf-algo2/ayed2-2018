#include <stdbool.h>
#include <stdlib.h>

#include "stack.h"

/* node_t the type of pointers to nodes
*/
typedef struct _node_t * node_t;

/* _node_t the type of nodes
*/
struct _node_t {
    stack_elem elem;
    node_t next;
};

/* one more indirection to deal with language c parameter passing properties
*/
typedef struct _stack_t {
    node_t the_stack;
} _stack_t;

stack_t create_stack() {
    stack_t s = (stack_t)malloc(sizeof(_stack_t));
    s->the_stack = NULL;
    return (s);
}

void empty_stack(stack_t s) {
    while (!is_empty_stack(s)) {
        pop(s);
    }
}

/* pre: !is_full_stack(s) */
void push(stack_elem e, stack_t s) {
/* needs implementation */
}

/* pre: !is_empty_stack(s) */
stack_elem top(stack_t s) {
/* needs implementation */
}

/* pre: !is_empty_stack(s) */
void pop(stack_t s) {
/* needs implementation */
}

bool is_empty_stack(stack_t s) {
/* needs implementation */
}

bool is_full_stack(stack_t s) {
    return (s == NULL && s != NULL);  //always returns false (trick to make the compiler happy)
}

void destroy_stack(stack_t s) {
    while (!is_empty_stack(s)) {
        pop(s);
    }
    free(s);
}

