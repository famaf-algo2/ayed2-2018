#include <stdbool.h>
#include <stdlib.h>

#include "queue.h"

/* node_t the type of pointers to nodes
*/
typedef struct _node_t * node_t;

/* _node_t the type of nodes
*/
struct _node_t {
    queue_elem elem;
    node_t next;
};

/* define this way if you will use circular linked lists
*/
typedef struct _queue_t {
    node_t the_last;
} _queue_t;

/****************************/
/*            OR            */
/****************************/

/* define this way if you will linked lists with pointer to the first and last nodes
*/
typedef struct _queue_t {
    node_t start;
    node_t end;
} _queue_t;

queue_t create_queue() {
/* needs implementation */
}

void empty_queue(queue_t q) {
/* needs implementation */
}

/* pre: !is_full_queue(q) */
void enqueue(queue_t q, queue_elem e) {
/* needs implementation */
}

/* pre: !is_empty_queue(q) */
queue_elem first(queue_t q) {
/* needs implementation */
}

/* pre: !is_empty_queue(q) */
void dequeue(queue_t q) {
/* needs implementation */
}

bool is_empty_queue(queue_t q) {
/* needs implementation */
}

bool is_full_queue(queue_t q) {
/* needs implementation */
}

void destroy_queue(queue_t q) {
/* needs implementation */
}

