#include <stdbool.h>
#include <stdlib.h>

#include "stack.h"

#define MAX_STACK 100

typedef struct _stack_t {
    unsigned int size;
    stack_elem elems[MAX_STACK];
} _stack_t;

stack_t create_stack() {
    stack_t s = (stack_t)malloc(sizeof(_stack_t));
    empty_stack(s);
    return (s);
}

void empty_stack(stack_t s) {
    s->size = 0;
}

/* pre: !is_full_stack(s) */
void push(stack_elem e, stack_t s) {
/* needs implementation */
}

/* pre: !is_empty_stack(s) */
stack_elem top(stack_t s) {
/* needs implementation */
}

/* pre: !is_empty_stack(s) */
void pop(stack_t s) {
/* needs implementation */
}

bool is_empty_stack(stack_t s) {
/* needs implementation */
}

bool is_full_stack(stack_t s) {
/* needs implementation */
}

void destroy_stack(stack_t s) {
/* needs implementation */
}

