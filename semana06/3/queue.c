#include <stdbool.h>
#include <stdlib.h>

#include "queue.h"

#define MAX_QUEUE 100000

typedef struct _queue_t {
    unsigned int start;
    unsigned int size;
    queue_elem elems[MAX_QUEUE];
} _queue_t;

queue_t create_queue() {
/* needs implementation */
}

void empty_queue(queue_t q) {
/* needs implementation */
}

/* pre: !is_full_queue(q) */
void enqueue(queue_t q, queue_elem e) {
/* needs implementation */
}

/* pre: !is_empty_queue(q) */
queue_elem first(queue_t q) {
/* needs implementation */
}

/* pre: !is_empty_queue(q) */
void dequeue(queue_t q) {
/* needs implementation */
}

bool is_empty_queue(queue_t q) {
/* needs implementation */
}

bool is_full_queue(queue_t q) {
/* needs implementation */
}

void destroy_queue(queue_t q) {
/* needs implementation */
}

