-- TAD PCola

module TADPCola(PCola,vacía,encolar,es_vacía,primero,decolar) where

-- CONSTRUCTORES

data Ord e => PCola e = Vacía
                      | Encolar (PCola e) e
                      deriving (Eq,Show)

-- AXIOMAS
-- Encolar (Encolar q e1) e2 = Encolar (Encolar q e2) e1

-- Invariante: ordenado de menor a mayor

vacía :: Ord e => PCola e
vacía = Vacía

encolar :: Ord e => PCola e -> e -> PCola e
encolar Vacía e = Encolar Vacía e
encolar (Encolar q f) e | f <= e = Encolar (Encolar q f) e
                        | otherwise = Encolar (encolar q e) f

-- OPERACIONES

es_vacía :: Ord e => PCola e -> Bool
primero :: Ord e => PCola e -> e        -- se aplica solo a una cola no Vacía
decolar :: Ord e => PCola e -> PCola e   -- se aplica solo a una cola no Vacía

-- ECUACIONES

es_vacía Vacía = True
es_vacía (Encolar q e) = False

primero Vacía = error "No se puede obtener el primero de la cola Vacía"
primero (Encolar q e) = e

decolar Vacía = error "No se puede decolar la cola Vacía"
decolar (Encolar q e) = q

