-- TAD Natural

module TADNatural (Natural, ceroN, sucesorN, es_ceroN, masN, porN, alaN, mayorN, anteriorN, menosN, máximoN) where

-- CONSTRUCTORES

data Natural = Cero | Sucesor Natural
             deriving (Show,Eq)

-- ECUACIONES ENTRE CONSTRUCTORES
-- No hay

ceroN = Cero
sucesorN = Sucesor

-- OPERACIONES

es_ceroN :: Natural -> Bool
masN :: Natural -> Natural -> Natural
porN :: Natural -> Natural -> Natural
alaN :: Natural -> Natural -> Natural
mayorN :: Natural -> Natural -> Bool
anteriorN :: Natural -> Natural -- sólo se aplica a naturales no Cero
menosN :: Natural -> Natural -> Natural
máximoN :: Natural -> Natural -> Natural

-- ECUACIONES

es_ceroN Cero = True
es_ceroN _ = False

masN Cero m = m
masN (Sucesor n) m = Sucesor (masN n m)

porN Cero m = Cero
porN (Sucesor n) m = masN m (porN n m)

alaN n Cero = Sucesor Cero
alaN n (Sucesor m) = porN n (alaN n m)

mayorN Cero _ = False
mayorN _ Cero = True
mayorN (Sucesor n) (Sucesor m) = mayorN n m

anteriorN (Sucesor n) = n

menosN n Cero = n
menosN Cero m = Cero
menosN (Sucesor n) (Sucesor m) = menosN n m

máximoN n Cero = n
máximoN Cero m = m
máximoN (Sucesor n) (Sucesor m) = Sucesor (máximoN n m)


