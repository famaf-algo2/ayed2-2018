-- TAD Entero

module TADEntero (Entero, posE, ceroE, negE, es_posE, es_ceroE, es_negE, opuestoE, absE, masE, porE, alaE, mayorE, menosE) where

import TADNatural

-- CONSTRUCTORES

data Entero = Pos Natural | Neg Natural
            deriving Show

-- ECUACIONES ENTRE CONSTRUCTORES
-- Pos ceroN == Neg ceroN

posE = Pos
ceroE = Pos ceroN
negE = Neg

-- OPERACIONES

es_posE :: Entero -> Bool
es_ceroE :: Entero -> Bool
es_negE :: Entero -> Bool
absE :: Entero -> Natural
opuestoE :: Entero -> Entero
masE :: Entero -> Entero -> Entero
porE :: Entero -> Entero -> Entero
alaE :: Entero -> Natural -> Entero
mayorE :: Entero -> Entero -> Bool
menosE :: Entero -> Entero -> Entero

-- ECUACIONES

-- las tres primeras son sencillas gracias al invariante

es_posE (Pos n) = n /= ceroN
es_posE _ = False

es_ceroE (Pos n) = es_ceroN n
es_ceroE (Neg n) = es_ceroN n

es_negE (Neg n) = n /= ceroN
es_negE _ = False

absE (Pos n) = n
absE (Neg n) = n

opuestoE (Pos n) = Neg n
opuestoE (Neg n) = Pos n

masE (Pos n) (Pos m) = Pos (masN n m)
masE (Pos n) (Neg m) | mayorN n m = Pos (menosN n m)
                     | otherwise = Neg (menosN m n)
masE (Neg n) (Pos m) | mayorN n m = Neg (menosN n m)
                     | otherwise = Pos (menosN m n)
masE (Neg n) (Neg m) = Neg (masN n m)

porE (Pos n) (Pos m) = Pos (porN n m)
porE (Pos n) (Neg m) = Neg (porN n m)
porE (Neg n) (Pos m) = Neg (porN n m)
porE (Neg n) (Neg m) = Pos (porN n m)

alaE z n | es_ceroN n = Pos (sucesorN ceroN)
         | otherwise = porE z (alaE z (anteriorN n))

mayorE (Pos n) (Pos m) = mayorN n m
mayorE (Pos n) (Neg m) = n /= ceroN || m /= ceroN
mayorE (Neg n) (Pos m) = False
mayorE (Neg n) (Neg m) = mayorN m n

menosE z w = masE z (opuestoE w)

instance Eq Entero where
  Pos n == Pos m = n == m
  Neg n == Neg m = n == m
  Pos n == Neg m = n == ceroN && m == ceroN
  Neg n == Pos m = n == ceroN && m == ceroN

