-- TAD Polinomio

module TADPolinomio (Polinomio, nuloP, monomioP, es_nuloP, opuestoP, gradoP, coefP, restoP, masP, evalP) where

import TADNatural
import TADEntero

-- CONSTRUCTORES

data Polinomio = Nulo | Monomio Entero Natural Polinomio
               deriving Show

-- ECUACIONES ENTRE CONSTRUCTORES
-- Monomio c e (Monomio d e p) = Monomio (masE c d) e p
-- Monomio ceroE e p = p
-- Monomio c e (Monomio d f p) = Monomio d f (Monomio c e p)

-- En la notación habitual de monomios como cx^e, estas ecuaciones se
-- escribirían, respectivamente:
-- c x^e + d x^e + p = (c+d) x^e + p
-- 0 x^e + p = p
-- c x^e + d x^f + p = d x^f + c x^e + p

nuloP = Nulo
monomioP = Monomio

-- OPERACIONES

es_nuloP :: Polinomio -> Bool
opuestoP :: Polinomio -> Polinomio
gradoP :: Polinomio -> Natural -- no está definido para el polinomio nulo
coefP :: Polinomio -> Natural -> Entero
restoP :: Polinomio -> Natural -> Polinomio
masP :: Polinomio -> Polinomio -> Polinomio
evalP :: Polinomio -> Entero -> Entero

-- ECUACIONES

es_nuloP Nulo = True
es_nuloP p@(Monomio _ e _) = es_ceroE (coefP p e) && es_nuloP (restoP p e)

opuestoP Nulo = Nulo
opuestoP (Monomio c e p) = Monomio (opuestoE c) e (opuestoP p)

gradoP p | es_nuloP p = error "No se puede obtener el grado del polinomio nulo"
gradoP p@(Monomio _ e q) | es_ceroE (coefP p e) = gradoP (restoP q e)
                         | es_nuloP (restoP q e) = e
                         | otherwise = e `máximoN` gradoP (restoP q e)
 
coefP Nulo n = ceroE
coefP (Monomio c e p) n | e == n = c `masE` coefP p n
                        | otherwise = coefP p n

restoP Nulo n = Nulo
restoP (Monomio c e p) n | e == n = restoP p n
                         | otherwise = Monomio c e (restoP p n)

masP Nulo q = q
masP (Monomio c e p) q = Monomio c e (masP p q)

evalP Nulo z = ceroE
evalP (Monomio c e p) z = porE c (alaE z e) `masE` evalP p z

instance Eq Polinomio where
  Nulo == q = es_nuloP q
  p@(Monomio c e _) == q = coefP p e == coefP q e && restoP p e == restoP q e

