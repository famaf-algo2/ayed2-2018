-- TAD Booleano

module TADBooleano (Booleano, verdadero, falso, yB, oB, noB, vale) where

-- CONSTRUCTORES

data Booleano = Verdadero
              | Falso
              deriving (Eq, Show)

verdadero = Verdadero
falso = Falso

-- OPERACIONES

yB :: Booleano -> Booleano -> Booleano
oB :: Booleano -> Booleano -> Booleano
noB :: Booleano -> Booleano
vale :: Booleano -> Bool

-- ECUACIONES

yB Verdadero x  = x
yB Falso x = Falso

oB Verdadero x = Verdadero
oB Falso x = x

noB Verdadero = Falso
noB Falso = Verdadero

vale Verdadero = True
vale Falso = False



