-- TAD Natural

module TADNatural (Natural, cero, sucesor, es_cero, mas, por, ala, mayor, anterior, menos, máximo) where

import TADBooleano

-- CONSTRUCTORES

data Natural = Cero | Sucesor Natural
             deriving (Show,Eq)

cero = Cero
sucesor = Sucesor

-- OPERACIONES

es_cero :: Natural -> Booleano
mas :: Natural -> Natural -> Natural
por :: Natural -> Natural -> Natural
ala :: Natural -> Natural -> Natural
mayor :: Natural -> Natural -> Booleano
anterior :: Natural -> Natural -- sólo se aplica a naturales no Cero
menos :: Natural -> Natural -> Natural
máximo :: Natural -> Natural -> Natural

-- ECUACIONES

es_cero Cero = verdadero
es_cero _ = falso

mas Cero m = m
mas (Sucesor n) m = Sucesor (mas n m)

por Cero m = Cero
por (Sucesor n) m = mas m (por n m)

ala n Cero = Sucesor Cero
ala n (Sucesor m) = por n (ala n m)

mayor Cero _ = falso
mayor _ Cero = verdadero
mayor (Sucesor n) (Sucesor m) = mayor n m

anterior (Sucesor n) = n

menos n Cero = n
menos Cero m = Cero
menos (Sucesor n) (Sucesor m) = menos n m

máximo n Cero = n
máximo Cero m = m
máximo (Sucesor n) (Sucesor m) = Sucesor (máximo n m)


