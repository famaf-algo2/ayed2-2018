-- TAD Lista

module TADLista (Lista, vacíaL, consL, es_vacíaL, cabezaL, colaL, concatL, largoL, puntoL, flechaL, snocL) where

import TADNatural

-- CONSTRUCTORES

data Lista e = Vacía | Cons e (Lista e)
             deriving (Show, Eq)

-- ECUACIONES ENTRE CONSTRUCTORES
-- no hay

vacíaL = Vacía
consL = Cons

-- OPERACIONES

es_vacíaL :: Lista e -> Bool
cabezaL :: Lista e -> e -- sólo se aplica a listas no vacías
colaL :: Lista e -> Lista e
concatL :: Lista e -> Lista e -> Lista e
largoL :: Lista e -> Natural
puntoL :: Lista e -> Natural -> e -- sólo se aplica a naturales menores que el largo de la lista
flechaL :: Lista e -> Natural -> Lista e
snocL :: Lista e -> e -> Lista e

-- ECUACIONES

es_vacíaL Vacía = True
es_vacíaL _ = False

cabezaL (Cons e l) = e

colaL (Cons e l) = l

concatL Vacía l2 = l2
concatL (Cons e l1) l2 = Cons e (concatL l1 l2)

largoL Vacía = ceroN
largoL (Cons e l) = sucesorN (largoL l)

puntoL (Cons e l) n | es_ceroN n = e
                    | otherwise = puntoL l (anteriorN n)

flechaL Vacía n = Vacía
flechaL l@(Cons _ l1) n | es_ceroN n = l
                        | otherwise = flechaL l1 (anteriorN n)

snocL Vacía f = Cons f Vacía
snocL (Cons e l) f = Cons e (snocL l f)

