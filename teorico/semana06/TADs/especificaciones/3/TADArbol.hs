-- TAD Árbol Binario

module TADArbol (Arbol, vacíoA, nodoA, es_vacíoA, raízA, izqA, derA, alturaA, posA, puntoA, flechaA) where

import TADNatural
import TADLista
import TADConjunto
import TADDirectiva

-- CONSTRUCTORES

data Arbol e = Vacío | Nodo (Arbol e) e (Arbol e)
             deriving (Show, Eq)

-- ECUACIONES ENTRE CONSTRUCTORES
-- no hay

vacíoA = Vacío
nodoA = Nodo

-- OPERACIONES

es_vacíoA :: Arbol e -> Bool
raízA :: Arbol e -> e -- sólo se aplica a árboles no vacíos
izqA :: Arbol e -> Arbol e -- sólo se aplica a árboles no vacíos
derA :: Arbol e -> Arbol e -- sólo se aplica a árboles no vacíos
alturaA :: Arbol e -> Natural
posA :: Arbol e -> Conjunto (Lista Directiva)
puntoA :: Arbol e -> Lista Directiva -> e -- sólo se aplica a listas de directivas que no se escapan del árbol
flechaA :: Arbol e -> Lista Directiva -> Arbol e

-- ECUACIONES

es_vacíoA Vacío = True
es_vacíoA _ = False

raízA (Nodo i r d) = r
izqA (Nodo i r d) = i
derA (Nodo i r d) = d

alturaA Vacío = ceroN
alturaA (Nodo i _ d) = sucesorN (alturaA i `máximoN` alturaA d)

posA Vacío = vacíoC
posA (Nodo i r d) = singC vacíaL `unionC` imagenC (consL izqD) (posA i) `unionC` imagenC (consL derD) (posA d)

puntoA (Nodo i r d) dirs | es_vacíaL dirs = r
                         | es_izqD (cabezaL dirs) = puntoA i (colaL dirs)
                         | es_derD (cabezaL dirs) = puntoA d (colaL dirs)

flechaA Vacío _ = Vacío
flechaA a@(Nodo i _ d) dirs | es_vacíaL dirs = a
                            | es_izqD (cabezaL dirs) = flechaA i (colaL dirs)
                            | es_derD (cabezaL dirs) = flechaA d (colaL dirs)

