-- TAD Directiva

module TADDirectiva (Directiva, izqD, derD, es_izqD, es_derD) where

-- CONSTRUCTORES

data Directiva = Izq | Der
               deriving (Show,Eq)

-- ECUACIONES ENTRE CONSTRUCTORES
-- No hay

izqD = Izq
derD = Der

-- OPERACIONES

es_izqD :: Directiva -> Bool
es_derD :: Directiva -> Bool

-- ECUACIONES

es_izqD Izq = True
es_izqD _ = False

es_derD Der = True
es_derD _ = False


