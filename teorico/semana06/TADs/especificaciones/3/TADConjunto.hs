-- TAD Conjunto Finito

module TADConjunto (Conjunto, vacíoC, consC, es_vacíoC, elemC, unionC, interC, singC, sinC, imagenC) where

-- CONSTRUCTORES

data Conjunto e = Vacío | Cons e (Conjunto e)
                deriving Show

-- ECUACIONES ENTRE CONSTRUCTORES
-- Cons e (Cons e c) == Cons e c
-- Cons e (Cons f c) == Cons f (Cons e c)

vacíoC = Vacío
consC = Cons

-- OPERACIONES

es_vacíoC :: Conjunto e -> Bool
elemC :: Eq e => e -> Conjunto e -> Bool
unionC :: Conjunto e -> Conjunto e -> Conjunto e
interC :: Eq e => Conjunto e -> Conjunto e -> Conjunto e
singC :: e -> Conjunto e
sinC :: Eq e => Conjunto e -> e -> Conjunto e
imagenC :: (e -> a) -> Conjunto e -> Conjunto a

-- ECUACIONES

es_vacíoC Vacío = True
es_vacíoC _ = False

elemC e Vacío = False
elemC e (Cons f c) = e == f || elemC e c

unionC Vacío d = d
unionC (Cons e c) d = Cons e (unionC c d)

interC Vacío d = Vacío
interC (Cons e c) d | elemC e d = Cons e (interC c d)
                    | otherwise = interC c d

singC e = consC e vacíoC

sinC Vacío _ = Vacío
sinC (Cons e c) f | e == f = sinC c f
                  | otherwise = Cons e (sinC c f)

imagenC f Vacío = Vacío
imagenC f (Cons e c) = Cons (f e) (imagenC f c)

instance Eq e => Eq (Conjunto e) where
  Vacío == Vacío = True
  Vacío == Cons _ _ = False
  c@(Cons e _) == d = elemC e d && sinC c e == sinC d e


