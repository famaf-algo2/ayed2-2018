Ejercicio 1: Completar la implementación del TAD map como un árbol binario de búsqueda.

En la carpeta Ayed2-2018/semana09/lab/ejercicios/1
disponés -entre otros- de los siguientes archivos:

dict.c : contiene una implementación del TAD Diccionario. Un diccionario asocia
         palabras con sus definiciones. Para ello se vale del TAD Mapeo que es más
         general, asocia claves con valores. Abrí el archivo y estudialo.
map.c : contiene una IMPLEMENTACIÓN INCOMPLETA del TAD Mapeo utilizando Árboles Binarios de Búsqueda (ABB).
        Tu trabajo en este proyecto es completarla.
string.c : contiene una implementación del TAD String, que se usa para las palabras y definiciones.
           Abrí el archivo y estudialo.
main.c : el programa principal es un interfaz para poder cargar diccionarios, agregar y eliminar
         palabras, etc.
Makefile : contiene una lista de requisitos para compilar y ejecutar.

El ejercicio es completar map.c. Observá todos los TADs involucrados. Todos ellos manejan
explícitamente la memoria. Es importante observar no solo cuándo es necesario crear un nodo
del ABB, sino también cuándo es necesario liberarlo, y en qué casos es necesario también
generar o liberar espacios de memoria para las claves y/o valores del map.

SE RECOMIENDA dedicar un tiempo para estudiar todos los archivos involucrados y así entender
el desarrollo en general.

SE RECOMIENDA implementar las operaciones del TAD Mapeo una por una, testeando cada una de ellas.
Esto es posible ya que, a pesar de estar incompleto, todo puede compilarse y ejecutarse

make run

y jugar siguiendo las opciones del menú. Por ejemplo, ingresar l y luego el archivo ../input/small.dic
o cualquier otro de la carpeta ../input.
 
Luego puede compilarse y ejecutarse con valgrind 

make run-valgrind

Finalmente, averiguar como funciona el comando 'make'.  

