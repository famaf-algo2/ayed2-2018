#include <assert.h>
#include <stdlib.h>
#include "map.h"

struct _node_t {
    map_t left;
    map_t right;
    key_t key;
    value_t value;
};

map_t map_empty() {
    return (NULL);
}

static map_t create_node(key_t key, value_t value, map_t left, map_t right) {
    map_t node = NULL;
    /* needs implementation */

    // just to make gcc happy. Remove it when implementing map_contains!
    if (key == NULL && value == NULL && left == NULL && right == NULL) {;}
    return (node);
}

static map_t free_node(map_t node) {
    /* needs implementation */
    if (node == NULL) {;}    // just to make gcc happy. Remove it when implementing map_contains!
    return (NULL);
}

map_t map_put(map_t map, key_t key, value_t value) {
    assert(key != NULL);
    assert(value != NULL);
    map_t node = map;

    /* needs implementation */

    node = create_node(key, value, NULL, NULL);  // just to make gcc happy. Remove it when implementing map_put!
    return (node);
}

value_t map_get(map_t map, key_t key) {
    assert(key != NULL);
    value_t value = NULL;

    /* needs implementation */

    if (map == NULL) {;}   // just to make gcc happy. Remove it when implementing map_get!
    return (value);
}

bool map_contains(map_t map, key_t key) {
    assert(key != NULL);
    /* needs implementation */

    if (map == NULL) {;}   // just to make gcc happy. Remove it when implementing map_contains!
    return (false);
}

map_t map_remove(map_t map, key_t key) {
    map_t result = map;
    if (map != NULL) {
        if (key_eq(map->key, key)) {
            if (map->left == NULL) {
                result = map->right;
                map = free_node(map);
            } else {
                map_t father = NULL;
                map_t max = map->left;
                while (max->right != NULL) {
                    father = max;
                    max = max->right;
                }
                if (father == NULL) {
                    max->right = map->right;
                    map = free_node(map);
                    result = max;
                } else {
                    map->key = key_destroy(map->key);
                    map->value = value_destroy(map->value);
                    map->key = max->key;
                    map->value = max->value;
                    father->right = max->left;
                    free(max);
                }
            } 
        } else if(key_less(map->key, key)) {
            map->right = map_remove(map->right, key);
        } else {
            map->left = map_remove(map->left, key);
        }
    }
    return (result);
}

map_t map_destroy(map_t map) {
    /* needs implementation */
    return (map);
}

void map_dump(map_t map, FILE *file) {
    if (map != NULL) {
        map_dump(map->left, file);
        key_dump(map->key, file);
        fprintf(file, ": ");
        value_dump(map->value, file);
        fprintf(file, "\n");
        map_dump(map->right, file);
    }
}
